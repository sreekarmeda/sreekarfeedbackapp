package com.example.win.androidrealmfinalpoc;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText user_name;
    private EditText password;
    private Button Login_Button;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Login_Button = (Button) findViewById(R.id.login_button);
        LoginButton();
    }

    public void LoginButton(){
        context = this;
        Login_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(context,User_Details_Screen.class);
                startActivity(i);
            }
        });
    }
}
