package com.example.win.androidrealmfinalpoc;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.win.androidrealmfinalpoc.Model.UserDetails;

import io.realm.Realm;

public class User_Details_Screen extends AppCompatActivity {
    private EditText user_Name;
    private EditText Country_Name;
    private EditText State_Name;
    private EditText CapitaName;

    private Button SaveButton;
    private Button ViewButton;

    private Context context;
    Realm realm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user__details__screen);

        user_Name = (EditText) findViewById(R.id.details_User_name);
        Country_Name = (EditText) findViewById(R.id.details_country_name);
        State_Name = (EditText) findViewById(R.id.details_state_name);
        CapitaName = (EditText) findViewById(R.id.Details_capitalName);

        SaveButton = (Button) findViewById(R.id.save_Details_button);
        ViewButton = (Button) findViewById(R.id.View_Details_button);

         realm = realm.getDefaultInstance();

        saveUserDetails();
        ShowAllUserDetails();

    }

    public void saveUserDetails(){
        SaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                writeToRealm(user_Name.getText().toString().trim(),Country_Name.getText().toString().trim(),State_Name.getText().toString().trim(),CapitaName.getText().toString().trim());
            }
        });
    }

    public void writeToRealm(String username,String countryName,String StateName,String capitalName){
        realm.beginTransaction();
        UserDetails userDetails = new UserDetails(username,countryName,StateName,capitalName);
        realm.copyToRealm(userDetails);
        realm.commitTransaction();
    }

    public void ShowAllUserDetails() {
        context = this;
        ViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context,View_All_User_Details.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }
        });
    }
}
