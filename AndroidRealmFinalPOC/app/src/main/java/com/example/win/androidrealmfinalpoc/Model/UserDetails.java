package com.example.win.androidrealmfinalpoc.Model;

import io.realm.RealmObject;

/**
 * Created by Win on 30-10-2017.
 */

public class UserDetails extends RealmObject {

    private String UserName;
    private String UserCountry;
    private String UserState;
    private String UserCapital;

    public UserDetails(String userName, String userCountry, String userState, String userCapital) {
        UserName = userName;
        UserCountry = userCountry;
        UserState = userState;
        UserCapital = userCapital;
    }

    public UserDetails() {
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getUserCountry() {
        return UserCountry;
    }

    public void setUserCountry(String userCountry) {
        UserCountry = userCountry;
    }

    public String getUserState() {
        return UserState;
    }

    public void setUserState(String userState) {
        UserState = userState;
    }

    public String getUserCapital() {
        return UserCapital;
    }

    public void setUserCapital(String userCapital) {
        UserCapital = userCapital;
    }
}
